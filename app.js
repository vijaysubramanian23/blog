'use strict';


let hapi = require('hapi'),
    RegisterPublicRoutes = require('./lib/routes/register-public-routes'),
    RegisterPrivateRoutes = require('./lib/routes/register-private-routes'),
    registerPublicRoutes,
    RedisClient = require('./lib/redis/redis-client'),
    nconf = require('nconf'),
    dependencies = {},
    serviceConfig = require('./config/service-config'),
    config = require('./config/config'),
    server,
    portConfig,
    inert = require('inert'),
    vision = require('vision'),
    DbConnect = require('./lib/dbDrivers/connectDriver'); 




class AppLoader {
    constructor() {
        nconf.argv().env();
        if (nconf.get('overrideConfig')) {
            overrides = require(nconf.get('overrideConfig'));
            nconf.overrides(overrides);
        }
        nconf.defaults(require('./config/config'));
        config = require('./config/config');
        serviceConfig = require('./config/service-config');
        server = new hapi.Server({
            "host": "localhost",
            "port": config.server.port.http,
            "routes": {
                "cors": {
                    "origin": ['http://localhost:3000']
                }
            }
            
        });
        portConfig = config.server.port;
        dependencies.redisClient = new RedisClient(dependencies, serviceConfig);
        dependencies.dbClient = new DbConnect();
        this.registerPublicRoutes = new RegisterPublicRoutes(dependencies, serviceConfig);
    }

    async myServer() {
        try {
           // await server.register(RegisterPublicRoutes);
           // await server.register([
             //   vision,
               // inert,
            //]);
            this.registerPublicRoutes.registerRoutes(server)

            await server.start();
            console.log(`Hapi server running at ${server.info.uri}`);

        } catch (err) {
            console.log("Hapi error starting server", err);
        }
    }

}

module.exports = AppLoader ;