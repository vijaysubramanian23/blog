var joi = require('joi');

const AuthSchema = joi.object({
    userName     : joi.string().min(3).max(20).required(),
    password     : joi.string().min(3).max(20).required()
})

const UserSchema = joi.object({
    userName     : joi.string().required(),
    bio          : joi.string().optional(),
    interests    : joi.array().items(joi.string()).optional(),
    followers    : joi.array().optional(),
    following    : joi.array().optional(),
    bookmarks    : joi.array().optional()
})

const Posts = joi.object({
    userName     : joi.string().optional(),
    postId       : joi.string().optional(),
    title        : joi.string().min(3).max(20).required(),
    content      : joi.string().min(10).required(),
    tags         : joi.array().items(joi.string()).optional()
})

const Comments = joi.object({
    userName     : joi.string().optional(),
    postId       : joi.string().optional(),
    content      : joi.string().required()
})

const PostUpdate = joi.object({
    postId       : joi.string().optional(),
    uCount       : joi.number().optional(),
    dCount       : joi.number().optional(),
    cCount       : joi.number().optional()

})

module.exports = {
    AuthSchema,
    UserSchema,
    Posts,
    Comments,
    PostUpdate
}