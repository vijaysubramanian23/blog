'use strict';

const redis = require('redis'),
  co = require('co'),
  wrapper = require('co-redis'),
  MILLISECONDS = 1000,
  Redlock = require('redlock');

class RedisClient {
  constructor (dependencies, config, requestContext) {
    this.config = config ;
    this.createClient();
    this.isRedisReady = false;
    this.redlock = new Redlock([this.redisClient], {
      driftFactor: 0.01,
      retryCount: 10,
      retryDelay: 1000
    });
    this.lockTtl = config.redisLockTtl || 10000;
  }

  createClient () {
    const me = this;
    me.redisClient = redis.createClient(
      me.config.redis.port,
      me.config.redis.host,
      {
        retry_strategy: (options) => {
          if (options.total_retry_time > ((me.config.redis.totalRetryTimeInSeconds * MILLISECONDS) || (60 * 60 * MILLISECONDS))) {
           // me.errorv2('RedisClient', 'retry_strategy', options.error, { redisHost: me.config.redis.host });
          }
          return Math.min(options.attempt * 100, me.config.redis.maxReconnectWaitTime || 2000);
        }
      }
    );

    me.redisClient.on('ready', () => {
      me.isRedisReady = true;
    });

    me.redisClient.on('error', (err) => {
    });

    me.redisClient.on('reconnecting', () => {
    });

    me.redisCoClient = wrapper(me.redisClient);
  }

  lock(key) {
    let me = this;
    return co(function *() {
      return yield me.redlock.lock(key, me.lockTtl);
    });
  }

  set(key, val, expireTime=60*60) {
    let me = this;
    return co(function *(){
      return yield me.redisCoClient.set(key, val, 'EX', expireTime);
    });
  }

  get(key) {
    let me = this;
    return co(function *(){
      return yield me.redisCoClient.get(key);
    })
  }

  del(key) {
    let me = this;
    return co(function *(){
      return yield me.redisCoClient.del(key);
    })
  }

  keys(keyPattern) {
    let me = this;
    return co(function *(){
      return yield me.redisCoClient.keys(keyPattern);
    })
  }

  flushdb() {
    let me = this;
    return co(function *(){
      return yield me.redisCoClient.flushdb();
    })
  }
}

module.exports = RedisClient;
