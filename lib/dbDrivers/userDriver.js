const DbSchema = require('../schema/database/db-schema')
const _        = require('lodash')
const uuid     = require('uuid')
const joi      = require('joi')

class UserDriver {
    constructor() {
        this.AuthSchema = DbSchema.AuthSchema
        this.UserSchema = DbSchema.UserSchema
    }

    async insertAuthSchema(dbClient,id,authDetails){
        
        var query = "insert into auth_schema (id,data) values ($1,$2)"
        let options = [id,authDetails]
        try{
            return await dbClient.query(query ,options)
        }
        catch(err){
            throw err
        }
    }

    async insertUserSchema(dbClient,id,userDetails){

        var query = "insert into user_schema (id,data) values ($1,$2)"
        let options = [id,userDetails]
        try{
            return await dbClient.query(query ,options)
        }
        catch(err){
            throw err
        }
    }

    async addNewUser(dbClient,authDetails,userDetails) {
        joi.validate(authDetails, this.AuthSchema)
        joi.validate(userDetails,this.UserSchema)
        try{
            let res = await this.getAuthDetails(dbClient,authDetails.userName)
            if(res.rowCount==0){ 
                var id = uuid.v4();
                await this.insertAuthSchema(dbClient,id,authDetails)
                await this.insertUserSchema(dbClient,id,userDetails)
                return true;     
                
            }
            //user exists
            console.log("user exists");
            return false;
            
        }
        catch(err){
            throw err
        }   
    }

    async getAuthDetails(dbClient,userName) {
        var query = "select data from auth_schema where data->>'userName'= $1"
        let options = [userName];
        try{
            return await dbClient.query(query ,options)
        }
        catch(err){
            throw err
        } 
    }

    async authenticateUser(dbClient,userName,password) {
        try{
            let res = await this.getAuthDetails(dbClient,userName)
            if(res){
                var auth = res.rows[0]
                if(auth != undefined){
                    if(auth.data.userName == userName && auth.data.password == password)
                        return true
                }
                return false
            }
            console.log(err);
            reject(err);
        }
        catch(err){
            throw err
        }
    }

    async getUserProfile(dbClient,userName) {
        var query = "(select t1.data from user_schema as t1 where t1.data->>'userName'= $1) UNION (SELECT t2.data from auth_schema as t2 WHERE t2.data->>'userName'= $1)"
        let options = [userName];
        try{
            let res = await dbClient.query(query ,options)
            //onsole.log(res);
            return res.rows;
        }
        catch(err){
            throw err
        } 
    }

    async updateAuthDetails(dbClient,authDetails){
        var query = "update auth_schema set data=$1 where data->>'userName'=$2"
        let options = [authDetails,authDetails.userName]
        try{
            let res = await dbClient.query(query ,options)
            console.log(res)
            return res;
        }
        catch(err){
            throw err
        }
    }

    async updateUserDetails(dbClient,userDetails){
        var query = "update user_schema set data=$1 where data->>'userName'=$2"
        let options = [userDetails,userDetails.userName]
        try{
            let res = await dbClient.query(query ,options)
            console.log(res)
            return res;
        }
        catch(err){
            throw err
        }
    }

    async editUserProfile(dbClient,authDetails,userDetails){
        joi.validate(authDetails, this.AuthSchema)
        joi.validate(userDetails,this.UserSchema)
        try{
            let res1 = await this.updateAuthDetails(dbClient,authDetails)
            let res2 = await this.updateUserDetails(dbClient,userDetails)
            if(res1.rowCount == 1 && res2.rowCount == 1){ 
                return true;         
            }
            //updation failed
            console.log("updation failed");
            return false;
            
        }
        catch(err){
            throw err
        }   
    }

}

module.exports = UserDriver