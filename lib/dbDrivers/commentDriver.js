const DbSchema = require('../schema/database/db-schema')
const _        = require('lodash')
const uuid     = require('uuid')
const joi      = require('joi')

class CommentDriver {
    constructor() {
        this.AuthSchema = DbSchema.AuthSchema
        this.UserSchema = DbSchema.UserSchema
        this.PostSchema = DbSchema.Posts
        this.CommentSchema = DbSchema.Comments
    }

    async getCommentsByPostId(dbClient,postId){
        var query = "select data from comments where data->>'postId'= $1"
        let options = [postId]
        try{
            let res =  await dbClient.query(query ,options)
            if(res.rowCount > 0){ 
                return res.rows;        
            }
            return false;
        }
        catch(err){
            throw err
        }
    }

    async getAllComments(dbClient){
        var query = "select data from comments"
        try{
            let res =  await dbClient.query(query)
            if(res.rowCount > 0){ 
                return res.rows;        
            }
            return false;
        }
        catch(err){
            throw err
        }
    }

    async isPostExists(dbClient,postId){
        var query = "select data from posts where data->>'postId'= $1"
        let options = [postId]
        try{
            return await dbClient.query(query ,options)
        }
        catch(err){
            throw err
        }
    }

    async addCommentUtil(dbClient,id,commentDetails){
        var query = "insert into comments (id,data) values ($1,$2)"
        let options = [id,commentDetails]
        try{
            let res =  await dbClient.query(query ,options)
            if(res.rowCount==1){ 
                return true;        
            }
        }
        catch(err){
            throw err
        } 
    }

    async addCommentByPostId(dbClient,postId,commentDetails){
        joi.validate(commentDetails, this.CommentSchema)
        let res = await this.isPostExists(dbClient,postId)
        if(res.rowCount == 1){
            var id = uuid.v4();
            commentDetails.postId = postId
               
            let result = await this.addCommentUtil(dbClient,id,commentDetails)
            if(result.rowCount==1){ 
                return true;        
            } 
        }   
        // post not exists
        console.log("add comment error");
        return false;       
    }

    async updateCommentDetails(dbClient,commentDetails){
        var query = "update comments set data=$1 where data->>'postId'=$2"
        let options = [commentDetails,commentDetails.postId]
        try{
            let res = await dbClient.query(query ,options)
            return res;
        }
        catch(err){
            throw err
        }
    }


    async editCommentByPostId(dbClient,commentDetails){
        joi.validate(commentDetails, this.CommentSchema)
        try{
            let res = await this.updateCommentDetails(dbClient,commentDetails)
            if(res.rowCount == 1){ 
                return true;         
            }
            //updation failed
            console.log("comment updation failed");
            return false;
            
        }
        catch(err){
            throw err
        }   
    }




}

module.exports = CommentDriver
