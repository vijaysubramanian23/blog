const DbSchema = require('../schema/database/db-schema')
const _        = require('lodash')
const uuid     = require('uuid')
const joi      = require('joi')

class PostDriver {
    constructor() {
        this.AuthSchema = DbSchema.AuthSchema
        this.UserSchema = DbSchema.UserSchema
        this.PostSchema = DbSchema.Posts
        this.PostUpdateSchema = DbSchema.PostUpdate
    }

    async getAllPosts(dbClient){
        var query = "select data from posts"
        try{
            let res =  await dbClient.query(query)
            return res.rows //array of posts
        }
        catch(err){
            throw err
        }
    }

    async getAllPostsByTag(dbClient,tag){
        var query = "select data from posts where (data->>'tags')::jsonb ? $1"
        let options = [tag]
        try{
            let res =  await dbClient.query(query,options)
            return res.rows //array of posts
        }
        catch(err){
            throw err
        }
    }

    async isTitleExists(dbClient,userName,title){
        var query = "select data from posts where data->>'userName'= $1 AND data->>'title' = $2"
        let options = [userName,title]
        try{
            return await dbClient.query(query ,options)
        }
        catch(err){
            throw err
        }
    }

    async addPostDetails(dbClient,id,postDetails){
        var query = "insert into posts (id,data) values ($1,$2)"
        let options = [id,postDetails]
        let udcDetails = {
            "postId" : postDetails.postId,
            "uCount" : 0,
            "dCount" : 0,
            "cCount" : 0
        }
        var udcQuery = "insert into post_update (id,data) values ($1,$2)"
        let udcOptions = [id,udcDetails]
        try{
            let res = await dbClient.query(udcQuery,udcOptions)
            return await dbClient.query(query ,options)
        }
        catch(err){
            throw err
        }
    }

    async addPost(dbClient,userName,postDetails){
        joi.validate(postDetails, this.PostSchema)
        try{
            let res = await this.isTitleExists(dbClient,userName,postDetails.title)
            if(res.rowCount == 0){
                var id = uuid.v4();
                postDetails.userName = userName
                postDetails.postId = uuid.v1();
                let result = await this.addPostDetails(dbClient,id,postDetails)
                if(result.rowCount==1){ 
                    return true;        
                }
            }
            //user exists
            console.log("add post error");
            return false;
            
        }
        catch(err){
            throw err
        }   
    }

    async getUserPosts(dbClient,userName) {
        var query = "(select data from posts where data->>'userName'= $1)"
        let options = [userName];
        try{
            let res = await dbClient.query(query ,options)
            return res.rows;
        }
        catch(err){
            throw err
        } 
    }


    async updatePostDetails(dbClient,postDetails){
        var query = "update posts set data=$1 where data->>'postId'=$2"
        let options = [postDetails,postDetails.postId]
        try{
            let res = await dbClient.query(query ,options)
            return res;
        }
        catch(err){
            throw err
        }
    }

    async editPostByPostId(dbClient,postDetails){
        joi.validate(postDetails, this.PostSchema)
        try{
            let res = await this.updatePostDetails(dbClient,postDetails)
            if(res.rowCount == 1){ 
                return true;         
            }
            //updation failed
            console.log("post updation failed");
            return false;
            
        }
        catch(err){
            throw err
        }   
    }

    async deletePostByPostId(dbClient,postId){
            var query = "delete from posts where data->>'postId' = $1"
            let options = [postId]
            try{
                let res = await dbClient.query(query ,options)
                if(res.rowCount == 1)
                    return true;
                return false;
            }
            catch(err){
                throw err
            }
    }

    async udcUtil(dbClient,type,udcDetails){
        if(type =='c'){//comment count
            var query = "select data->>'cCount' as ans from post_update where data->>'postId' = $1"
            let options = [udcDetails.postId]
            try{
                let res = await dbClient.query(query ,options)
                udcDetails.cCount = parseInt(res.rows[0].ans) + 1;
            }
            catch(err){
                throw err
            }
        }
        if(type == 'u'){//upvote count
            var query = "select data->>'uCount' as ans from post_update where data->>'postId' = $1"
            let options = [udcDetails.postId]
            try{
                let res = await dbClient.query(query ,options)
                udcDetails.uCount = parseInt(res.rows[0].ans) + 1;
            }
            catch(err){
                throw err
            }
        }
        if(type == 'd'){//downvote count
            var query = "select data->>'dCount' as ans from post_update where data->>'postId' = $1"
            let options = [udcDetails.postId]
            try{
                let res = await dbClient.query(query ,options)
                udcDetails.dCount = parseInt(res.rows[0].ans) + 1;
            }
            catch(err){
                throw err
            }
        }
        var query = "update post_update set data=$1 where data->>'postId'=$2"
        let options = [udcDetails,udcDetails.postId]
        try{
            let res = await dbClient.query(query ,options)
            return res;
        }
        catch(err){
            throw err
        }
    }

    async udcUpdate(dbClient,type,udcDetails){
        joi.validate(udcDetails, this.PostUpdateSchema)
        try{
            let res = await this.udcUtil(dbClient,type,udcDetails)
            if(res.rowCount == 1){ 
                return true;         
            }
            //updation failed
            console.log("post udc updation failed");
            return false;
            
        }
        catch(err){
            throw err
        }   
    }

}

module.exports = PostDriver