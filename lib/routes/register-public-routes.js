'use strict';

const joi = require('joi'),
    RedisClient = require('../redis/redis-client'),
    PublicRouteHandler = require('./public-route-handler'),
    UserDriver = require('../dbDrivers/userDriver'),
    PostDriver = require('../dbDrivers/postDriver'),
    CommentDriver = require('../dbDrivers/commentDriver'),
    DbSchema = require('../schema/database/db-schema');

    

class RegisterPublicRoutes {
    constructor(dependencies, config) {
        this.config = config;
        this.dependencies = dependencies;
        this.userDriver = new UserDriver();
        this.postDriver = new PostDriver();
        this.commentDriver = new CommentDriver();
        this.AuthSchema = DbSchema.AuthSchema
        this.UserSchema = DbSchema.UserSchema
        this.PostSchema = DbSchema.Posts
        this.CommentSchema = DbSchema.Comments
        this.UdcSchema = DbSchema.PostUpdate
    }

    registerRoutes(server) {

        server.route({
            method: 'GET',
            path: '/v1/hello/{user}',
            config: {
                handler: (request, h) => {

                    const user = request.params.user ? encodeURIComponent(request.params.user) : 'stranger';
                    return 'Hello, ' + user + '!';
                },
                description: 'Registers the request for an OTP creation',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate: {
                    params: {
                        // user: Joi.string().required().description('test'),
                        user: joi.string().required()
                    }
                }
            }
        });

        // server.route({
        //     method: 'GET',
        //     path: '/',
        //     config:{
        //         handler: (request, h) => {
        //             return '<html><head><title>Blog</title></head><body><h3>Welcome!</h3><br/>';
        //         }
        //     }
        // });

        server.route({
            method: 'POST',
            path: '/register',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    
                    let res = await this.userDriver.addNewUser(this.dependencies.dbClient,request.payload.authDetails,request.payload.userDetails);
                    if(res)
                        return h.response(res).code(201)
                     else
                        return h.response("false").code(400)
                         
                },
                description: 'user registration',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    payload : joi.object({
                        userDetails : this.UserSchema,
                        authDetails : this.AuthSchema
                    })
    
                }
            }
        });

        server.route({
            method: 'POST',
            path: '/login',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    
                    let res = await this.userDriver.authenticateUser(this.dependencies.dbClient,request.payload.authDetails.userName,request.payload.authDetails.password)
                    if(res){
                        console.log("login valid");
                        return h.response(res).code(200)
                    }
                    console.log("Login Invalid");
                    return h.response(res).code(401)
                    
                },
                description: 'user authentication',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    payload : joi.object({
                        authDetails : this.AuthSchema
                    })
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/{userName}/profile',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if(request.params.userName){
                        let res = await this.userDriver.getUserProfile(this.dependencies.dbClient,encodeURIComponent(request.params.userName))
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'Get user profile',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        userName: joi.string().required()
                    })
    
                }
            }
        });

        server.route({
            method: 'PUT',
            path: '/{userName}/profile',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if(request.params.userName){
                        let res = await this.userDriver.editUserProfile(this.dependencies.dbClient,request.payload.authDetails,request.payload.userDetails)
                        console.log("User profile updated")
                        return h.response("User profile updated").code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'Edit user profile',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        userName: joi.string().required()
                    })
    
                }
            }
        });

        server.route({
            method: 'POST',
            path: '/{userName}/posts/add',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    // try{
                    
                    // let isInCache = await redisClient.redisCoClient.get(request.payload.postDetails.title);
                    // console.log(isInCache);
                    // if(isInCache && isInCache.isSuccess)
                    // {
                    //     throw {
                    //         "message" : "Data already in redis Cache "
                    //     }
                    // }
                    // await redisClient.set(request.payload.postDetails.title, true, 20) ;
                    if(request.params.userName){
                        let res = await this.postDriver.addPost(this.dependencies.dbClient,encodeURIComponent(request.params.userName),request.payload.postDetails)
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                // }
                // catch(err){
                //     console.log(err);
                // }
                },
                description: 'Edit user profile',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        userName: joi.string().required()
                    }),
                    payload : joi.object({
                        postDetails : this.PostSchema,
                    })
    
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/posts',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                        let res = await this.postDriver.getAllPosts(this.dependencies.dbClient)
                        if(res){//array of posts
                            return h.response(res).code(200)
                        }
                        return h.response("false").code(400)
                },
                description: 'Get all posts',
                tags: ['api', 'alohomora', 'Otp', 'public']
            }
        });

        server.route({
            method: 'GET',
            path: '/posts/{tag}',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                        let res = await this.postDriver.getAllPostsByTag(this.dependencies.dbClient,encodeURIComponent(request.params.tag))
                        if(res){//array of posts
                            return h.response(res).code(200)
                        }
                        return h.response("false").code(400)
                },
                description: 'Get all posts',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        tag: joi.string().required()
                    })
                }
            }
        });
        server.route({
            method: 'GET',
            path: '/comments/{postId}',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                        let res = await this.commentDriver.getCommentsByPostId(this.dependencies.dbClient,encodeURIComponent(request.params.postId))
                        if(res){//array of commentss
                            return h.response(res).code(200)
                        }
                        return h.response("false").code(400)
                },
                description: 'Get all comments for postId',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        postId: joi.string().required()
                    })
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/comments',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                        let res = await this.commentDriver.getAllComments(this.dependencies.dbClient)
                        if(res){//array of commentss
                            return h.response(res).code(200)
                        }
                        return h.response("false").code(400)
                },
                description: 'Get all comments',
                tags: ['api', 'alohomora', 'Otp', 'public']
            }
        });


        server.route({
            method: 'PUT',
            path: '/posts/udcUpdate/{type}',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if(request.params.type){
                        let res = await this.postDriver.udcUpdate(this.dependencies.dbClient,encodeURIComponent(request.params.type),request.payload.udcDetails)
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'update upvotes, downvotes and comments count for postId',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        type         : joi.string().length(1).required() //Either comment(c) or upvote(u) or downvote(d)
                    }),
                    payload : joi.object({
                        udcDetails : this.UdcSchema
                    })
    
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/{userName}/posts',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if(request.params.userName){
                        let res = await this.postDriver.getUserPosts(this.dependencies.dbClient,encodeURIComponent(request.params.userName))
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'Get user posts',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        userName: joi.string().required()
                    })
    
                }
            }
        });

        server.route({
            method: 'PUT',
            path: '/{userName}/posts/{postId}',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if(request.params.userName && request.params.postId){
                        let res = await this.postDriver.editPostByPostId(this.dependencies.dbClient,request.payload.postDetails)
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'Edit particular post by postId',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        userName: joi.string().required(),
                        postId: joi.string().required()
                    }),
                    payload : joi.object({
                        postDetails : this.PostSchema
                    })
    
                }
            }
        });

        server.route({
            method: 'DELETE',
            path: '/{userName}/posts/{postId}',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if(request.params.userName && request.params.postId){
                        let res = await this.postDriver.deletePostByPostId(this.dependencies.dbClient,encodeURIComponent(request.params.postId))
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'Delete particular post by postId',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        userName: joi.string().required(),
                        postId: joi.string().required()
                    }),
                }
            }
        });

        server.route({
            method: 'POST',
            path: '/posts/{postId}/addComment',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if(request.params.postId){
                        let res = await this.commentDriver.addCommentByPostId(this.dependencies.dbClient,encodeURIComponent(request.params.postId),request.payload.commentDetails)
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'Add comment ',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        postId: joi.string().required()
                    }),
                    payload : joi.object({
                        commentDetails : this.CommentSchema
                    })
        
                }
            }
        });
        server.route({
            method: 'PUT',
            path: '/posts/{postId}/updateComment',
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                },
                handler: async (request, h) => {
                    if( request.params.postId){
                        let res = await this.commentDriver.editCommentByPostId(this.dependencies.dbClient,request.payload.commentDetails)
                        return h.response(res).code(200)
                    }
                    return h.response("false").code(400)
                },
                description: 'Edit particular comment by postId',
                tags: ['api', 'alohomora', 'Otp', 'public'],
                validate :{
                    params : joi.object({
                        postId: joi.string().required()
                    }),
                    payload : joi.object({
                        commentDetails : this.CommentSchema
                    })
    
                }
            }
        });


    }


}

module.exports = RegisterPublicRoutes;